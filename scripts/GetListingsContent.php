<?php

    // Get the Content from the sitemap and save to file to be extracted
    // To be run daily
	$rootpath = str_replace("\\", "/", dirname(__FILE__));

	require_once $rootpath . "/../support/web_browser.php";
	require_once $rootpath . "/../doh_web_browser.php";
	require_once $rootpath . "/../support/tag_filter.php";

	$web = new DOHWebBrowser();	
    $urls = array(
        "http://storage-pr.realtor.ca/sitemap/realtorsitemap/ListingSitemap1.xml",
        "http://storage-pr.realtor.ca/sitemap/realtorsitemap/ListingSitemap2.xml",
        "http://storage-pr.realtor.ca/sitemap/realtorsitemap/ListingSitemap3.xml",
        "http://storage-pr.realtor.ca/sitemap/realtorsitemap/ListingSitemap4.xml",
        "http://storage-pr.realtor.ca/sitemap/realtorsitemap/ListingSitemap5.xml"
	);

    $counter = 1;
    $title = "ListingContent/Draft/ListingSitemap";
    
    foreach ($urls as $url) {
        $result = $web->Process($url);

        if ($result["success"] == intval(1) || $result["response"]["code"] == 200)
        {
            $filename = $title . $counter . ".txt";
            $handle = fopen($filename,"w");

            fwrite($handle,$result['body']);
            fclose($handle);

            $counter++;
        }
    }
   
?>