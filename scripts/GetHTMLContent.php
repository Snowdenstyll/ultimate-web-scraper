<?php

    $rootpath = str_replace("\\", "/", dirname(__FILE__));

    require_once $rootpath . "/../support/web_browser.php";
    require_once $rootpath . "/../doh_web_browser.php";
    require_once $rootpath . "/../support/tag_filter.php";
	require_once $rootpath . "/../support/simple_html_dom.php";

	// Retrieve the standard HTML parsing array for later use.
	$htmloptions = TagFilter::GetHTMLOptions();
    $html = new simple_html_dom();

    $path = $rootpath . "/../HTML/test1.txt";
    $file_content = file_get_contents($path, true);
    //print_r($file_content);
    $html->load($file_content);

    /**
     * Get the Images
     */
    /* $highResPhotos = $html->find('div.royalSlider a[href]');
    foreach ($highResPhotos as $row)
    {
        echo "\t" . $row->href. "\n";
    } */

    /**
     * Get the object data
     */
    $pattern = "/(?s)(?<=GTMDataLayerCode).*?(?=DATALAYER)/";
    preg_match($pattern, $file_content, $matches); 
    //print_r($matches);

    $pattern2 = "/(?s)(?=site).*?(?=script)/";
    preg_match($pattern2, $matches[0], $matches2); 
    //print_r($matches2);
    
    $pattern3 = "/(?s)(?<=property: {).*?(?=})/";
    preg_match($pattern3, $matches2[0], $matches3); 
    //print_r($matches3);
    
    $homeObject = json_decode(json_encode($matches3[0]), true);
    $homeObjectArr = (explode("\r\n                                ", $homeObject));

    $result = [];
    foreach($homeObjectArr as $t) {
        if (!empty(trim($t))) {
            $t = str_replace(",", " ", $t);
            $r = explode(":", $t);
            $result[$r[0]] = $r[1];
        }   
    }


    /**
     * Get Extra Data 
     */
    $patternData = "/(?s)(?<=application\/ld\+json\">).*?(?<=})/";
    preg_match_all($patternData, $file_content, $matchesData); 
    //print_r($matchesData[0][0]);

    $patternData2 = "/(?s)(?<={).*?(?=\"offers\")/";
    preg_match($patternData2, $matchesData[0][0], $matchesData2); 
    //print_r($matchesData2);
    
    $dataObject = json_decode(json_encode($matchesData2[0]), true);
    //print_r($dataObject);
    $dataObjectArr = (explode('",', $dataObject));
        
    $skipArr =["context"];
    $dataResult = [];
    foreach($dataObjectArr as $t) {
        if (!empty(trim($t))) {
            //$t = str_replace(",", " ", $t);
            if (!strpos($t,"@context")) {
                print($t . " => ");
                $r = explode(":", $t);
                $dataResult[trim($r[0])] = trim($r[1]) . '"';
            }
        }   
    }
    //print_r($dataResult);

?>